﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectifEvents : MonoBehaviour
{
    public Sprite on_touch;
    public Collider2D projectile;
    public GameObject next_screen;


    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider == projectile)
        {
            GetComponent<SpriteRenderer>().sprite = on_touch;
            projectile.gameObject.SetActive(false);
            next_screen.SetActive(true);
        }
    }
}
