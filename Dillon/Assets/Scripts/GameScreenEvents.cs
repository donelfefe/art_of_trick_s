﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameScreenEvents : MonoBehaviour
{
    private Text score;
    public LevelChanger levelChanger;

    private void Awake()
    {
        score = GetComponentInChildren<Text>();
    }

    void Update()
    {
        score.text = "Score\r\n" + ScoreManager.SCORE;
    }

    public void replay()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        gameObject.SetActive(false);
    }

    public void next()
    {
        levelChanger.FadeToNextLevel();
        gameObject.SetActive(false);
    }
}
