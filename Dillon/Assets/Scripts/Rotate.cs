﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Rotate : MonoBehaviour,IPointerDownHandler,IPointerUpHandler
{
    public Player angel;

    [Range(-1,1)]
    public int direction;

    private bool click = false;

    private float currentWidth;
    private float angelWidth;

    public void OnPointerDown(PointerEventData eventData)
    {
        click = true;
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        click = false;
    }

    private void Awake()
    {
        currentWidth = GetComponent<BoxCollider2D>().bounds.size.x;
        angelWidth = angel.GetComponent<SpriteRenderer>().bounds.size.x;

        transform.position = new Vector3((angel.transform.position.x + (((currentWidth + angelWidth) / 2)) * direction), angel.transform.position.y, angel.transform.position.z);
    }

    private void Update()
    {
            MoveRelativeToAngel();

        if (click)
        {
            angel.turn(direction);
        }
    }

    private void MoveRelativeToAngel()
    {
        transform.position = new Vector3((angel.transform.position.x + (((currentWidth + angelWidth) / 2)) * direction), angel.transform.position.y, angel.transform.position.z);
    }
}
