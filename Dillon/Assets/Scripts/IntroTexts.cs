﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class IntroTexts
{
    public static List<string> GetTexts() {
        List<string> texts = new List<string>();

        texts.Add("Golem");
        texts.Add("Je t'ai invoqué car j'ai besoin de toi.");
        texts.Add("L'enfant perd de ses nuances...");
        texts.Add("Il a besoin de tes couleurs!");
        texts.Add("Maintenant va.");

        return texts;
    }


}
