﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class WallEvents : MonoBehaviour
{
    public Collider2D projectile;
    public GameObject replay_screen;

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider == projectile)
        {
            projectile.gameObject.SetActive(false);
            replay_screen.SetActive(true);
        }
    }
}
