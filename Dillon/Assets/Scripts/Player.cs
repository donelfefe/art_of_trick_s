﻿using System;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class Player : MonoBehaviour,IPointerDownHandler,IPointerUpHandler
{
    public Projectile projectile;
    public float speed;

    private Vector3 move;
    public float rotationSpeed;

    public Transform shootPanel; 

    [HideInInspector]
    public bool clickToDrag = false;
    private bool isClicked = false;
    public float time_to_click = 1;
    private float time;

    public void OnPointerDown(PointerEventData eventData)
    {
        clickToDrag = true;
        isClicked = true;
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        clickToDrag = false;
    }

    private void Awake()
    {
        time = time_to_click;    
    }

    private void Update()
    {
        if (isClicked)
        {
            if (clickToDrag)
            {
                time -= Time.deltaTime;

               // transform.position = position;
            }
            else
            {
                if (time > 0)
                {
                    projectile.GetComponent<CircleCollider2D>().enabled = true;
                    projectile.shoot(transform);
                }
                time = time_to_click;
                isClicked = false;
            }
        }

        shootPanel.position = Camera.main.WorldToScreenPoint(transform.position);
        
    }

    public void budge(int direction)
    {
        this.transform.Translate(Vector3.right * direction * Time.deltaTime, Space.World);
    }

    public void turn(int direction)
    {
        this.transform.RotateAround(this.transform.position, Vector3.forward, -direction * Time.deltaTime * this.rotationSpeed);
    }
}
