﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Move : MonoBehaviour,IPointerDownHandler,IPointerUpHandler
{
    public Player angel;

    [Range(-1, 1)]
    public int direction;

    private bool click = false;

    public void OnPointerDown(PointerEventData eventData)
    {
        click = true;
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        click = false;
    }

    private void Awake()
    {
    }

    private void Update()
    {
        if (click)
        {
            angel.budge(direction);
        }
    }
}
