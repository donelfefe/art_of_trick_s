﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    public float projectionSpeed;
    [Range(50,200)]
    public float rotationSpeed;
    private int baseRotationSpeed = 200;
    private float rotationVariation;
    private bool asCollide = false;
    private bool asEasterEgg = false;
    private int rotationDirection = 1;
    private ScoreManager ScoreManager;

    private void Awake()
    {
        ScoreManager = new ScoreManager();
        ScoreManager.collisionCounter = 0;
    }

    private void Update()
    {
        if (asCollide)
        {
            
            transform.Rotate(transform.forward * Time.deltaTime * rotationVariation * rotationDirection);
        }
    }

    public void shoot(Transform transform)
    {

        gameObject.GetComponent<Rigidbody2D>().AddForce(transform.up * projectionSpeed, ForceMode2D.Impulse);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        Debug.Log("From - "+ScoreManager.basePoints);
        Debug.Log(ScoreManager.collisionCounter);
        Debug.Log(ScoreManager.SCORE+" / ");
        rotationDirection *= -1;
        rotationVariation = Random.Range(baseRotationSpeed - rotationSpeed, baseRotationSpeed + rotationSpeed);
        asCollide = true;
        switch (collision.gameObject.layer){
            case 9:
                ScoreManager.collisionCounter++;
                if (ScoreManager.collisionCounter > 0)
                {
                    ScoreManager.basePoints += (ScoreManager.collisionCounter * ScoreManager.basePoints) / Mathf.Log(ScoreManager.basePoints * 10);
                }
                break;
            case 11:
                ScoreManager.SCORE = 1;
                break;
            case 12:
                if (asEasterEgg)
                {
                    ScoreManager.SCORE += Mathf.CeilToInt(ScoreManager.basePoints) * 2;
                } else
                {
                    ScoreManager.SCORE += Mathf.CeilToInt(ScoreManager.basePoints);
                }
                break;
            case 13:
                asEasterEgg = true;
                break;
        }
    }
}
