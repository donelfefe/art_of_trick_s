﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ConfigurationTrigger : MonoBehaviour
{
    public static string CONFIGURATION_SAVE = "IS_CONF_SHOWING";

    public Text text;

    public void Trigger()
    {
        gameObject.SetActive(!gameObject.activeSelf);
        if (gameObject.activeSelf)
        {
            text.text = "cacher";
        } else
        {
            text.text = "montrer";
            PlayerPrefs.SetInt(CONFIGURATION_SAVE, 0);
        }
        PlayerPrefs.Save();
    }

    private void Awake()
    {
        if(PlayerPrefs.GetInt(CONFIGURATION_SAVE) == 0)
        {
            gameObject.SetActive(false);
            text.text = "montrer";
        }
    }
}
