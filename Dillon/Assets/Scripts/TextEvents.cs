﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class TextEvents : MonoBehaviour, IPointerClickHandler
{
    Text text;
    int clickCounter;

    public Animator face;
    public LevelChanger levelChanger;

    public void OnPointerClick(PointerEventData eventData)
    {
     //   face.Play("Talking");

        clickCounter++;
        if (clickCounter < IntroTexts.GetTexts().Count)
        {
            text.text = IntroTexts.GetTexts()[clickCounter];
        }

        if (clickCounter == IntroTexts.GetTexts().Count)
        {
            levelChanger.FadeToLevel(1);
        }
    }

    void Awake()
    {
        text = GetComponent<Text>();
        clickCounter = -1;
    }

    private void Update()
    {
        face.transform.position = Vector3.MoveTowards(face.transform.position, new Vector3(face.transform.position.x, 2.7f, 5.23f),0.1f);
    }
}
